FROM adoptopenjdk/openjdk11:latest
VOLUME /app
COPY target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]
